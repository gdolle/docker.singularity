FROM golang:1.11-alpine 
RUN apk add --update git alpine-sdk automake libtool linux-headers libarchive-dev util-linux-dev libuuid openssl-dev gawk sed; \
    mkdir -p $GOPATH/src/github.com/sylabs; \
    cd $GOPATH/src/github.com/sylabs; \
    git clone https://github.com/sylabs/singularity; \
    cd $GOPATH/src/github.com/sylabs/singularity; \
    ./mconfig; \
    make -C ./builddir; \
    make -C ./builddir install; \
    rm -rf $GOPATH/src/github.com/sylabs
